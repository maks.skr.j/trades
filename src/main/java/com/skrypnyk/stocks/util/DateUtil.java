package com.skrypnyk.stocks.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.SneakyThrows;

/**
 * {@author maksymskrypnyk} since 0.1.0
 **/
public class DateUtil {

    @SneakyThrows
    public static Date buildDate(final String date) {
        final SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        return sf.parse(date);
    }

    public static boolean isWithinRange(final Date dateToCheck, final Date fromDateFilter, final Date toDateFilter) {
        return (dateToCheck.after(fromDateFilter) && dateToCheck.before(toDateFilter))
                || dateToCheck.equals(fromDateFilter)
                || dateToCheck.equals(toDateFilter);
    }
}
