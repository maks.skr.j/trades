package com.skrypnyk.stocks.service;

import com.skrypnyk.stocks.dal.dao.UserDao;
import com.skrypnyk.stocks.entity.api.UserApi;
import com.skrypnyk.stocks.entity.db.UserDb;
import com.skrypnyk.stocks.entity.mapper.UserMapper;
import com.skrypnyk.stocks.exceptions.UserWasNotFoundException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * {@author maksymskrypnyk} since 0.1.0
 **/
@Slf4j
@Service
@RequiredArgsConstructor
public class UserService {

    @NonNull
    private final UserDao userDao;
    @NonNull
    private final UserMapper userMapper;

    public UserApi getUserById(final Long userId) {
        log.debug("Getting user by id: {}", userId);
        final UserDb userDb = userDao.findById(userId)
                                     .orElseThrow(() -> new UserWasNotFoundException(
                                             String.format("User was not found by ID: %s", userId)));

        return userMapper.toUserApi(userDb);
    }
}
