package com.skrypnyk.stocks.service;

import static com.skrypnyk.stocks.util.DateUtil.isWithinRange;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.skrypnyk.stocks.dal.dao.TradeDao;
import com.skrypnyk.stocks.entity.api.StockPriceApi;
import com.skrypnyk.stocks.entity.api.TradeApi;
import com.skrypnyk.stocks.entity.db.TradeDb;
import com.skrypnyk.stocks.entity.enums.TradeType;
import com.skrypnyk.stocks.entity.mapper.TradeMapper;
import com.skrypnyk.stocks.exceptions.DateRangeException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

/**
 * Represent service methods to work with {@link TradeApi}
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 **/
@Slf4j
@Service
@RequiredArgsConstructor
public class TradeService {

    @NonNull
    private final TradeDao tradeDao;
    @NonNull
    private final TradeMapper tradeMapper;

    /**
     * Create new {@link TradeApi}
     *
     * @param tradeApi - to create
     * @return created {@link TradeApi}
     */
    public TradeApi createNewTrade(final TradeApi tradeApi) {
        log.debug("Creating new Trade: {}", tradeApi.toString());
        final TradeDb createdTradeDb = tradeDao.save(tradeMapper.toTradeDb(tradeApi));
        return tradeMapper.toTradeApi(createdTradeDb);
    }

    /**
     * Get all existing {@link TradeApi}
     *
     * @return List of {@link TradeApi}
     */
    public List<TradeApi> getAllTrades() {
        log.debug("Getting all trades");
        final List<TradeDb> tradeDbs = tradeDao.getAllTrades();
        return tradeMapper.toTradeApi(tradeDbs);
    }

    /**
     * Get {@link TradeApi} by UserId
     *
     * @param userId - to find trades
     * @return List of {@link TradeApi}
     */
    public List<TradeApi> getTradesByUserId(final Long userId) {
        log.debug("Getting trades by userId: {}", userId);
        final List<TradeDb> tradeDbs = tradeDao.getTradesByUserId(userId);

        return tradeMapper.toTradeApi(tradeDbs);
    }

    /**
     * Get all {@link TradeApi} for specific stockSymbol and tradeType in given date range
     *
     * @param stockSymbol - to find trades
     * @param tradeType   - to find trades
     * @param startDate   - to filter trades
     * @param endDate     - to filter trades
     * @return List of {@link TradeApi}
     */
    public List<TradeApi> getTradesByParams(final String stockSymbol,
                                            final TradeType tradeType,
                                            final Date startDate,
                                            final Date endDate) {
        log.debug("Getting trades by params: stockSymbol: {}, tradeType: {}, fromDate: {}, toDate:{}",
                  stockSymbol,
                  tradeType,
                  startDate,
                  endDate);

        final List<TradeDb> tradeDbs = tradeDao.getTradesByStockSymbolAndTradeType(stockSymbol, tradeType);

        if (CollectionUtils.isEmpty(tradeDbs)) {
            return Collections.emptyList();
        }

        return filterTradesByDate(tradeMapper.toTradeApi(tradeDbs), startDate, endDate);
    }

    /**
     * Get stockPrice by stockSymbol in given date range
     *
     * @param stockSymbol - to find stockPrice
     * @param startDate   - to filter trades
     * @param endDate     - to filter trades
     * @return {@link StockPriceApi}
     */
    public StockPriceApi getStockPriceBySymbolInDateRange(final String stockSymbol,
                                                          final Date startDate,
                                                          final Date endDate) {
        log.debug("Getting stock price by symbol: {} in date range from: {} to: {}",
                  stockSymbol,
                  startDate,
                  endDate);
        final List<TradeDb> tradeDbs = tradeDao.getStockBySymbol(stockSymbol);
        final List<TradeApi> tradeApis = filterTradesByDate(tradeMapper.toTradeApi(tradeDbs), startDate, endDate);

        return StockPriceApi.builder()
                            .stockSymbol(stockSymbol)
                            .highestPrice(getHighestPrice(tradeApis))
                            .lowestPrice(getLowestPrice(tradeApis))
                            .build();
    }

    /**
     * Delete all {@link TradeApi}
     */
    public void deleteAllTrades() {
        log.debug("Deleting all trades");
        tradeDao.deleteAll();
    }

    private List<TradeApi> filterTradesByDate(final List<TradeApi> trades,
                                              final Date startDate,
                                              final Date endDate) {
        final List<TradeApi> matchedTrades = trades.stream()
                                                   .filter(trade -> isWithinRange(trade.getTradeTimestamp(),
                                                                                  startDate,
                                                                                  endDate))
                                                   .collect(Collectors.toList());

        if (CollectionUtils.isEmpty(matchedTrades)) {
            throw new DateRangeException("There are no trades in the given date range");
        }

        return matchedTrades;
    }

    private double getLowestPrice(final List<TradeApi> tradeApis) {
        return tradeApis.stream()
                        .map(TradeApi::getStockPrice)
                        .min(Comparator.naturalOrder())
                        .get();
    }

    private double getHighestPrice(final List<TradeApi> tradeApis) {
        return tradeApis.stream()
                        .map(TradeApi::getStockPrice)
                        .max(Comparator.naturalOrder())
                        .get();
    }
}
