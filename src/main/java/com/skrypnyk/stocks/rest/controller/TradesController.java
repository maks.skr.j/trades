package com.skrypnyk.stocks.rest.controller;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.List;

import javax.validation.Valid;

import com.skrypnyk.stocks.entity.api.TradeApi;
import com.skrypnyk.stocks.service.TradeService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller class represents endpoints for Trades requests
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 **/
@Slf4j
@RestController
@RequestMapping(value = "/trades")
@RequiredArgsConstructor
public class TradesController {

    @NonNull
    private final TradeService tradeService;

    /**
     * Create new {@link TradeApi}
     *
     * @param tradeApi - to create
     * @return created {@link TradeApi}
     */
    @RequestMapping(method = POST)
    public TradeApi createNewTrade(final @Valid @RequestBody TradeApi tradeApi) {
        log.info("Creating new Trade: {}", tradeApi.toString());
        return tradeService.createNewTrade(tradeApi);
    }

    /**
     * Return all existing trades
     *
     * @return List of {@link TradeApi}
     */
    @RequestMapping(method = GET)
    public List<TradeApi> getAllTrades() {
        log.info("Getting all trades");
        return tradeService.getAllTrades();
    }

    /**
     * Return trades with specific UserId
     *
     * @param userId - to find trades
     * @return list of {@link TradeApi}
     */
    @RequestMapping(value = "/users/{userId}", method = GET)
    public List<TradeApi> getTradesByUserId(final @PathVariable("userId") Long userId) {
        log.info("Getting trades by userId: {}", userId);
        return tradeService.getTradesByUserId(userId);
    }

    /**
     * Deleting all {@link TradeApi}
     */
    @RequestMapping(method = DELETE)
    public void deleteAllTrades() {
        log.info("Deleting all trades");
        tradeService.deleteAllTrades();
    }
}
