package com.skrypnyk.stocks.rest.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.Date;
import java.util.List;

import com.skrypnyk.stocks.entity.api.StockPriceApi;
import com.skrypnyk.stocks.entity.api.TradeApi;
import com.skrypnyk.stocks.entity.enums.TradeType;
import com.skrypnyk.stocks.service.TradeService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller class represents endpoints for Stocks requests
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 **/
@Slf4j
@RestController
@RequestMapping(value = "/stocks")
@RequiredArgsConstructor
public class StocksController {

    @NonNull
    private final TradeService tradeService;

    /**
     * Get all {@link TradeApi} by stockSymbol and tradeType in given date range
     *
     * @param stockSymbol - to find Trades
     * @param tradeType   - to find Trades
     * @param startDate   - to filter Trades
     * @param endDate     - to filter Trades
     * @return List of {@link TradeApi}
     */
    @RequestMapping(value = "/{stockSymbol}/trades", params = {"type", "start", "end"}, method = GET)
    public List<TradeApi> getTradesByParams(final @PathVariable("stockSymbol") String stockSymbol,
                                            final @RequestParam("tradeType") TradeType tradeType,
                                            final @RequestParam("start") Date startDate,
                                            final @RequestParam("end") Date endDate) {
        log.info("Getting trades by params: stockSymbol: {}, tradeType: {}, fromDate: {}, toDate:{}",
                 stockSymbol,
                 tradeType,
                 startDate,
                 endDate);
        return tradeService.getTradesByParams(stockSymbol, tradeType, startDate, endDate);
    }

    /**
     * Get stock prices by stockSymbol on given date range
     *
     * @param stockSymbol - to find stock
     * @param startDate   - to filter trades
     * @param endDate     - to filter trades
     * @return {@link StockPriceApi}
     */
    @RequestMapping(value = "/{stockSymbol}/price", params = {"start", "end"}, method = GET)
    public StockPriceApi getStockPriceBySymbolInDateRange(final @PathVariable("stockSymbol") String stockSymbol,
                                                          final @RequestParam("start") Date startDate,
                                                          final @RequestParam("end") Date endDate) {
        log.info("Getting stock price by symbol: {} in date range from: {} to: {}",
                 stockSymbol,
                 startDate,
                 endDate);
        return tradeService.getStockPriceBySymbolInDateRange(stockSymbol, startDate, endDate);
    }
}
