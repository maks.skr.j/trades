package com.skrypnyk.stocks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StocksApplication {

    public static void main(final String[] args) {
        SpringApplication.run(StocksApplication.class, args);

        System.setProperty("user.timezone", "EST");
    }
}
