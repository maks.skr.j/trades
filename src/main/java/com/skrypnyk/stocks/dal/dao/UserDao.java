package com.skrypnyk.stocks.dal.dao;

import com.skrypnyk.stocks.entity.db.UserDb;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Represents methods with requests to User DB
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 **/
@Repository
public interface UserDao extends CrudRepository<UserDb, Long> {
}
