package com.skrypnyk.stocks.dal.dao;

import java.util.List;

import com.skrypnyk.stocks.entity.db.TradeDb;
import com.skrypnyk.stocks.entity.enums.TradeType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Represents methods with requests to Trade DB
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 **/
@Repository
public interface TradeDao extends CrudRepository<TradeDb, Long> {

    /**
     * Get all {@link TradeDb}
     *
     * @return List of {@link TradeDb}
     */
    default List<TradeDb> getAllTrades() {
        return (List<TradeDb>) findAll();
    }

    /**
     * Get all {@link TradeDb} with specific userId
     *
     * @param userId - to find trades
     * @return List of {@link TradeDb}
     */
    List<TradeDb> getTradesByUserId(Long userId);

    /**
     * Get {@link TradeDb} by stockSymbol and tradeType
     *
     * @param stockSymbol - to find trades
     * @param tradeType   - to find trades
     * @return List of {@link TradeDb}
     */
    @Query("select trade from TradeDb trade where trade.stockSymbol = :stockSymbol and trade.tradeType = :tradeType")
    List<TradeDb> getTradesByStockSymbolAndTradeType(@Param("stockSymbol") String stockSymbol,
                                                     @Param("tradeType") TradeType tradeType);

    /**
     * Get {@link TradeDb} by stockSymbol
     *
     * @param stockSymbol - to find trades
     * @return List of {@link TradeDb}
     */
    @Query("select trade from TradeDb trade where trade.stockSymbol = :stockSymbol")
    List<TradeDb> getStockBySymbol(@Param("stockSymbol") String stockSymbol);
}
