package com.skrypnyk.stocks.exceptions;

/**
 * {@author maksymskrypnyk} since 0.1.0
 **/
public class UserWasNotFoundException extends RuntimeException {

    public UserWasNotFoundException(final String message) {
        super(message);
    }
}
