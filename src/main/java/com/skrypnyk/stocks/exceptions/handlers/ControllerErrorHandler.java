package com.skrypnyk.stocks.exceptions.handlers;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import java.util.Objects;

import com.skrypnyk.stocks.exceptions.ApiException;
import com.skrypnyk.stocks.exceptions.DateRangeException;
import com.skrypnyk.stocks.exceptions.UserWasNotFoundException;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Represents error handlers for thrown exceptions
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 **/
@ControllerAdvice
public class ControllerErrorHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = DateRangeException.class)
    protected ResponseEntity<Object> handleDateRangeException(final RuntimeException exception,
                                                              final WebRequest request) {
        final ApiException apiException = new ApiException(exception.getMessage());
        return handleExceptionInternal(exception, apiException, new HttpHeaders(), NOT_FOUND, request);
    }

    @ExceptionHandler(value = UserWasNotFoundException.class)
    protected ResponseEntity<Object> handleUserWasNotFoundException(final RuntimeException exception,
                                                                    final WebRequest request) {
        final ApiException apiException = new ApiException(exception.getMessage());
        return handleExceptionInternal(exception, apiException, new HttpHeaders(), NOT_FOUND, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException exception,
                                                                  final @NotNull HttpHeaders headers,
                                                                  final @NotNull HttpStatus status,
                                                                  final @NotNull WebRequest request) {
        final ApiException apiException = new ApiException(Objects.requireNonNull(exception.getBindingResult()
                                                                                           .getFieldError())
                                                                  .getDefaultMessage());
        return handleExceptionInternal(exception, apiException, new HttpHeaders(), BAD_REQUEST, request);
    }
}
