package com.skrypnyk.stocks.exceptions;

/**
 * Exception that should be thrown if no trades matched with date range
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 **/
public class DateRangeException extends RuntimeException {

    public DateRangeException(final String message) {
        super(message);
    }
}
