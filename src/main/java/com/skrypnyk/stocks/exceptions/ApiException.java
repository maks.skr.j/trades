package com.skrypnyk.stocks.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

/**
 * Api template for response with exception
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 **/
@Data
@AllArgsConstructor
public class ApiException {

    @NonNull
    private String message;
}
