package com.skrypnyk.stocks.entity.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Enum that represents all of available TradeTypes
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 */
public enum TradeType {
    @JsonProperty("buy") BUY,
    @JsonProperty("sell") SELL
}
