package com.skrypnyk.stocks.entity.deserializer;

import com.skrypnyk.stocks.entity.enums.TradeType;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Represent {@link String} to {@link TradeType} converter
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 **/
@Component
public class TradeTypeConverter implements Converter<String, TradeType> {
    @Override
    public TradeType convert(final String s) {
        return TradeType.valueOf(s.toUpperCase());
    }
}
