package com.skrypnyk.stocks.entity.deserializer;

import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Represent {@link String} to {@link Date} converter
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 **/
@Component
public class DateConverter implements Converter<String, Date> {
    @SneakyThrows
    @Override
    public Date convert(final @NotNull String stringDate) {
        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.parse(stringDate);
    }
}
