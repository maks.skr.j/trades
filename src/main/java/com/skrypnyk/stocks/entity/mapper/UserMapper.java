package com.skrypnyk.stocks.entity.mapper;

import com.skrypnyk.stocks.entity.api.UserApi;
import com.skrypnyk.stocks.entity.db.UserDb;
import lombok.NonNull;
import org.springframework.stereotype.Component;

/**
 * Represent methods for mapping {@link UserDb} to {@link UserApi} and vice versa
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 **/
@Component
public class UserMapper {

    /**
     * Map {@link UserApi} to {@link UserDb}
     *
     * @param userApi - to map
     * @return {@link UserDb}
     */
    public UserDb toUserDb(final @NonNull UserApi userApi) {
        return UserDb.builder()
                     .id(userApi.getId())
                     .name(userApi.getName())
                     .build();
    }

    /**
     * Map {@link UserDb} to {@link UserApi}
     *
     * @param userDb - to map
     * @return {@link UserApi}
     */
    public UserApi toUserApi(final @NonNull UserDb userDb) {
        return UserApi.builder()
                      .id(userDb.getId())
                      .name(userDb.getName())
                      .build();
    }
}
