package com.skrypnyk.stocks.entity.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.skrypnyk.stocks.entity.api.TradeApi;
import com.skrypnyk.stocks.entity.api.UserApi;
import com.skrypnyk.stocks.entity.db.TradeDb;
import com.skrypnyk.stocks.service.UserService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * Represent methods for mapping {@link TradeDb} to {@link TradeApi} and vice versa
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 **/
@Component
@RequiredArgsConstructor
public class TradeMapper {

    @NonNull
    private final UserService userService;

    /**
     * Map {@link TradeApi} to {@link TradeDb}
     *
     * @param tradeApi - to map
     * @return {@link TradeDb}
     */
    public TradeDb toTradeDb(final @NonNull TradeApi tradeApi) {
        return TradeDb.builder()
                      .id(tradeApi.getId())
                      .stockPrice(roundStockPrice(tradeApi.getStockPrice()))
                      .stockQuantity(tradeApi.getStockQuantity())
                      .stockSymbol(tradeApi.getStockSymbol())
                      .tradeType(tradeApi.getTradeType())
                      .userId(tradeApi.getUserApi().getId())
                      .build();
    }

    /**
     * Map {@link TradeDb} to {@link TradeApi}
     *
     * @param tradeDb - to map
     * @return {@link TradeApi}
     */
    public TradeApi toTradeApi(final @NonNull TradeDb tradeDb) {
        final UserApi userApi = userService.getUserById(tradeDb.getUserId());

        return TradeApi.builder()
                       .id(tradeDb.getId())
                       .stockPrice(tradeDb.getStockPrice())
                       .stockQuantity(tradeDb.getStockQuantity())
                       .stockSymbol(tradeDb.getStockSymbol())
                       .tradeType(tradeDb.getTradeType())
                       .tradeTimestamp(tradeDb.getTradeTimestamp())
                       .userApi(userApi)
                       .build();
    }

    /**
     * Map List of {@link TradeDb} to {@link TradeApi}
     *
     * @param tradeDbs - to map
     * @return List of {@link TradeApi}
     */
    public List<TradeApi> toTradeApi(final @NonNull List<TradeDb> tradeDbs) {
        return tradeDbs.stream()
                       .map(this::toTradeApi)
                       .collect(Collectors.toList());
    }

    private double roundStockPrice(final double initialPrice) {
        return Math.round(initialPrice * 100) / 100D;
    }
}
