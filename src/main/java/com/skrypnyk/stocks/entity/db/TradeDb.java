package com.skrypnyk.stocks.entity.db;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import com.skrypnyk.stocks.entity.enums.TradeType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Represents persistence version of Trade
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 **/
@Data
@Builder(toBuilder = true)
@Entity
@Table(name = "trades")
@AllArgsConstructor
@NoArgsConstructor
public class TradeDb {

    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private TradeType tradeType;

    @Column(name = "stock_symbol", nullable = false)
    private String stockSymbol;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "stock_quantity", nullable = false)
    private Integer stockQuantity;

    @Column(name = "stock_price", nullable = false)
    private Double stockPrice;

    @Column(name = "trade_timestamp", nullable = false, updatable = false)
    private Date tradeTimestamp;

    @PrePersist
    protected void onCreate() {
        tradeTimestamp = new Date();
    }
}
