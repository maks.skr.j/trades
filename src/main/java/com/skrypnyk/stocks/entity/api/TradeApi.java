package com.skrypnyk.stocks.entity.api;


import java.util.Date;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.skrypnyk.stocks.entity.enums.TradeType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Represent API entity of Trade
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 **/
@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class TradeApi {

    private Long id;

    @JsonProperty("type")
    @NotNull(message = "type field is required")
    private TradeType tradeType;

    @JsonProperty("user")
    @NotNull(message = "user field is required")
    private UserApi userApi;

    @JsonProperty("stock_symbol")
    @NotNull(message = "stock_symbol field is required")
    private String stockSymbol;

    @Min(value = 10, message = "stock_quantity should be more than 10")
    @Max(value = 30, message = "stock_quantity should be less than 30")
    @JsonProperty("stock_quantity")
    private int stockQuantity;

    @DecimalMin(value = "130.42", message = "stock_price should be more than 130.42")
    @DecimalMax(value = "195.65", message = "stock_price should be less than 195.65")
    @JsonProperty("stock_price")
    private double stockPrice;

    @JsonProperty("trade_timestamp")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date tradeTimestamp;
}
