package com.skrypnyk.stocks.entity.api;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Reproduce API entity for User
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserApi {

    private Long id;

    @NotNull(message = "name field is required")
    private String name;

}
