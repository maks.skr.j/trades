package com.skrypnyk.stocks.entity.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Represent API entity of Stock price
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StockPriceApi {

    private String stockSymbol;
    private double highestPrice;
    private double lowestPrice;
}
