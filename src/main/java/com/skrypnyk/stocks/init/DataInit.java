package com.skrypnyk.stocks.init;

import static com.skrypnyk.stocks.entity.enums.TradeType.BUY;
import static com.skrypnyk.stocks.entity.enums.TradeType.SELL;

import com.skrypnyk.stocks.dal.dao.TradeDao;
import com.skrypnyk.stocks.dal.dao.UserDao;
import com.skrypnyk.stocks.entity.db.TradeDb;
import com.skrypnyk.stocks.entity.db.UserDb;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * Common class for initiate date in DB
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 **/
@RequiredArgsConstructor
@Component
public class DataInit implements ApplicationRunner {
    private final UserDao userDao;
    private final TradeDao tradeDao;

    @Override
    public void run(final ApplicationArguments args) {
        final UserDb userDbToCreate1 = UserDb.builder().name("John").build();
        final UserDb userDbToCreate2 = UserDb.builder().name("Sem").build();
        final UserDb userDbToCreate3 = UserDb.builder().name("Sting").build();
        final UserDb userDbToCreate4 = UserDb.builder().name("Marshal").build();

        final UserDb userDb1 = userDao.save(userDbToCreate1);
        final UserDb userDb2 = userDao.save(userDbToCreate2);
        userDao.save(userDbToCreate3);
        userDao.save(userDbToCreate4);

        final TradeDb tradeDb1 = TradeDb.builder()
                                        .tradeType(BUY)
                                        .userId(userDb1.getId())
                                        .stockSymbol("AC")
                                        .stockQuantity(28)
                                        .stockPrice(162.17)
                                        .build();
        final TradeDb tradeDb2 = TradeDb.builder()
                                        .tradeType(SELL)
                                        .userId(userDb2.getId())
                                        .stockSymbol("AC")
                                        .stockQuantity(28)
                                        .stockPrice(100.17)
                                        .build();

        tradeDao.save(tradeDb1);
        tradeDao.save(tradeDb2);
    }
}
