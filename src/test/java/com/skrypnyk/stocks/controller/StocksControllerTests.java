package com.skrypnyk.stocks.controller;

import static com.skrypnyk.stocks.TestData.STOCK_PRICE_API_1;
import static com.skrypnyk.stocks.TestData.STOCK_PRICE_API_2;
import static com.skrypnyk.stocks.TestData.STOCK_PRICE_API_3;
import static com.skrypnyk.stocks.TestData.TRADE_API_1;
import static com.skrypnyk.stocks.TestData.TRADE_API_2;
import static com.skrypnyk.stocks.entity.enums.TradeType.BUY;
import static com.skrypnyk.stocks.util.DateUtil.buildDate;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.List;

import com.skrypnyk.stocks.entity.api.StockPriceApi;
import com.skrypnyk.stocks.entity.api.TradeApi;
import com.skrypnyk.stocks.exceptions.DateRangeException;
import com.skrypnyk.stocks.rest.controller.StocksController;
import com.skrypnyk.stocks.service.TradeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * Represent tests for {@link StocksController} functionality
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 **/
@ExtendWith(MockitoExtension.class)
public class StocksControllerTests {

    @InjectMocks
    private StocksController stocksController;

    @Mock
    private TradeService tradeServiceMock;

    @Test
    public void getTradesByParam_oneTradeMatched() {
        final Date from = buildDate("2020-01-12 12:12:12");
        final Date to = buildDate("2020-12-12 12:12:12");

        when(tradeServiceMock.getTradesByParams("AC", BUY, from, to)).thenReturn(singletonList(TRADE_API_2));

        final List<TradeApi> actualTradeApis = stocksController.getTradesByParams("AC", BUY, from, to);

        verify(tradeServiceMock).getTradesByParams("AC", BUY, from, to);
        verifyNoMoreInteractions(tradeServiceMock);

        assertThat(actualTradeApis).isEqualTo(singletonList(TRADE_API_2));
    }

    @Test
    public void getTradesByParam_severalTradesMatched() {
        final List<TradeApi> matchedTradeApis = asList(TRADE_API_1, TRADE_API_2);
        final Date from = buildDate("2020-01-12 12:12:12");
        final Date to = buildDate("2021-12-12 12:12:12");

        when(tradeServiceMock.getTradesByParams("AC", BUY, from, to)).thenReturn(matchedTradeApis);

        final List<TradeApi> actualTradeApis = stocksController.getTradesByParams("AC", BUY, from, to);

        verify(tradeServiceMock).getTradesByParams("AC", BUY, from, to);
        verifyNoMoreInteractions(tradeServiceMock);

        assertThat(actualTradeApis).isEqualTo(matchedTradeApis);
    }

    @Test
    public void getTradesByParam_noTradesByStockSymbol() {
        final Date from = buildDate("2020-01-12 12:12:12");
        final Date to = buildDate("2021-12-12 12:12:12");

        when(tradeServiceMock.getTradesByParams("DC", BUY, from, to)).thenReturn(emptyList());

        final List<TradeApi> actualTradeApis = stocksController.getTradesByParams("DC", BUY, from, to);

        verify(tradeServiceMock).getTradesByParams("DC", BUY, from, to);
        verifyNoMoreInteractions(tradeServiceMock);

        assertThat(actualTradeApis).isEmpty();
    }

    @Test
    public void getTradesByParam_noTradesByTradeType() {
        final Date from = buildDate("2020-01-12 12:12:12");
        final Date to = buildDate("2021-12-12 12:12:12");

        when(tradeServiceMock.getTradesByParams("AC", BUY, from, to)).thenReturn(emptyList());

        final List<TradeApi> actualTradeApis = stocksController.getTradesByParams("AC", BUY, from, to);

        verify(tradeServiceMock).getTradesByParams("AC", BUY, from, to);
        verifyNoMoreInteractions(tradeServiceMock);

        assertThat(actualTradeApis).isEmpty();
    }

    @Test
    public void getTradesByParam_noTradesInDateRange() {
        final Date from = buildDate("2020-01-12 12:12:12");
        final Date to = buildDate("2020-01-12 12:55:55");

        when(tradeServiceMock.getTradesByParams("AC", BUY, from, to)).thenReturn(emptyList());

        final List<TradeApi> actualTradeApis = stocksController.getTradesByParams("AC", BUY, from, to);

        verify(tradeServiceMock).getTradesByParams("AC", BUY, from, to);
        verifyNoMoreInteractions(tradeServiceMock);

        assertThat(actualTradeApis).isEmpty();
    }

    @Test
    public void getStockPriceBySymbolInDateRange_oneTradeMatched() {
        final Date from = buildDate("2020-01-12 12:12:12");
        final Date to = buildDate("2022-12-12 12:12:12");

        when(tradeServiceMock.getStockPriceBySymbolInDateRange("ACC", from, to)).thenReturn(STOCK_PRICE_API_3);

        final StockPriceApi actualStockPriceApi = stocksController.getStockPriceBySymbolInDateRange("ACC", from, to);

        verify(tradeServiceMock).getStockPriceBySymbolInDateRange("ACC", from, to);
        verifyNoMoreInteractions(tradeServiceMock);

        assertThat(actualStockPriceApi).isEqualTo(STOCK_PRICE_API_3);
    }

    @Test
    public void getStockPriceBySymbolInDateRange_oneTradeMatchedByDate() {
        final Date from = buildDate("2020-01-01 12:12:12");
        final Date to = buildDate("2020-12-30 12:12:12");

        when(tradeServiceMock.getStockPriceBySymbolInDateRange("AC", from, to)).thenReturn(STOCK_PRICE_API_2);

        final StockPriceApi actualStockPriceApi = stocksController.getStockPriceBySymbolInDateRange("AC", from, to);

        verify(tradeServiceMock).getStockPriceBySymbolInDateRange("AC", from, to);
        verifyNoMoreInteractions(tradeServiceMock);

        assertThat(actualStockPriceApi).isEqualTo(STOCK_PRICE_API_2);
    }

    @Test
    public void getStockPriceBySymbolInDateRange_severalTradesMatched() {
        final Date from = buildDate("2020-01-01 12:12:12");
        final Date to = buildDate("2021-12-30 12:12:12");

        when(tradeServiceMock.getStockPriceBySymbolInDateRange("AC", from, to)).thenReturn(STOCK_PRICE_API_1);

        final StockPriceApi actualStockPriceApi = stocksController.getStockPriceBySymbolInDateRange("AC", from, to);

        verify(tradeServiceMock).getStockPriceBySymbolInDateRange("AC", from, to);
        verifyNoMoreInteractions(tradeServiceMock);

        assertThat(actualStockPriceApi).isEqualTo(STOCK_PRICE_API_1);
    }

    @Test
    public void getStockPriceBySymbolInDateRange_noTradesMatchedByStockSymbol() {
        final Date from = buildDate("2000-01-01 12:12:12");
        final Date to = buildDate("2030-12-30 12:12:12");

        when(tradeServiceMock.getStockPriceBySymbolInDateRange("DC", from, to))
                .thenThrow(new DateRangeException("No trades in given data range"));

        assertThrows(DateRangeException.class, () -> stocksController.getStockPriceBySymbolInDateRange("DC", from, to));

        verify(tradeServiceMock).getStockPriceBySymbolInDateRange("DC", from, to);
        verifyNoMoreInteractions(tradeServiceMock);
    }

    @Test
    public void getStockPriceBySymbolInDateRange_noTradesMatchedByDateRange() {
        final Date from = buildDate("2020-01-01 12:12:12");
        final Date to = buildDate("2020-01-01 12:12:20");

        when(tradeServiceMock.getStockPriceBySymbolInDateRange("AC", from, to))
                .thenThrow(new DateRangeException("No trades in given data range"));

        assertThrows(DateRangeException.class, () -> stocksController.getStockPriceBySymbolInDateRange("AC", from, to));

        verify(tradeServiceMock).getStockPriceBySymbolInDateRange("AC", from, to);
        verifyNoMoreInteractions(tradeServiceMock);
    }
}
