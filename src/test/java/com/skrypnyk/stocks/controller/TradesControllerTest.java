package com.skrypnyk.stocks.controller;

import static com.skrypnyk.stocks.TestData.TRADE_API_1;
import static com.skrypnyk.stocks.TestData.TRADE_API_2;
import static com.skrypnyk.stocks.TestData.TRADE_API_3;
import static com.skrypnyk.stocks.TestData.TRADE_API_4;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.List;

import com.skrypnyk.stocks.entity.api.TradeApi;
import com.skrypnyk.stocks.exceptions.UserWasNotFoundException;
import com.skrypnyk.stocks.rest.controller.TradesController;
import com.skrypnyk.stocks.service.TradeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * Represent tests for {@link TradesController} functionality
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 **/
@ExtendWith(MockitoExtension.class)
public class TradesControllerTest {

    @InjectMocks
    private TradesController tradesController;

    @Mock
    private TradeService tradeServiceMock;

    @Test
    public void getAllTrades_severalTrades() {
        final List<TradeApi> allTradeApis = asList(TRADE_API_1, TRADE_API_2, TRADE_API_3, TRADE_API_4);

        when(tradeServiceMock.getAllTrades()).thenReturn(allTradeApis);

        final List<TradeApi> actualTradeApis = tradesController.getAllTrades();

        verify(tradeServiceMock).getAllTrades();
        verifyNoMoreInteractions(tradeServiceMock);

        assertThat(actualTradeApis).isEqualTo(allTradeApis);
    }

    @Test
    public void getAllTrades_oneTrade() {
        when(tradeServiceMock.getAllTrades()).thenReturn(singletonList(TRADE_API_1));

        final List<TradeApi> actualTradeApis = tradesController.getAllTrades();

        verify(tradeServiceMock).getAllTrades();
        verifyNoMoreInteractions(tradeServiceMock);

        assertThat(actualTradeApis).isEqualTo(singletonList(TRADE_API_1));
    }

    @Test
    public void getAllTrades_noTrades() {
        when(tradeServiceMock.getAllTrades()).thenReturn(emptyList());

        final List<TradeApi> actualTradeApis = tradesController.getAllTrades();

        verify(tradeServiceMock).getAllTrades();
        verifyNoMoreInteractions(tradeServiceMock);

        assertThat(actualTradeApis).isEmpty();
    }

    @Test
    public void getTradesByUserId_severalTrades() {
        final List<TradeApi> tradeApisMatchedWithUser = asList(TRADE_API_1, TRADE_API_2);

        when(tradeServiceMock.getTradesByUserId(1L)).thenReturn(tradeApisMatchedWithUser);

        final List<TradeApi> actualTradeApis = tradesController.getTradesByUserId(1L);

        verify(tradeServiceMock).getTradesByUserId(1L);
        verifyNoMoreInteractions(tradeServiceMock);

        assertThat(actualTradeApis).isEqualTo(tradeApisMatchedWithUser);
    }

    @Test
    public void getTradesByUserId_oneTrade() {
        when(tradeServiceMock.getTradesByUserId(2L)).thenReturn(singletonList(TRADE_API_3));

        final List<TradeApi> actualTradeApis = tradesController.getTradesByUserId(2L);

        verify(tradeServiceMock).getTradesByUserId(2L);
        verifyNoMoreInteractions(tradeServiceMock);

        assertThat(actualTradeApis).isEqualTo(singletonList(TRADE_API_3));
    }

    @Test
    public void getTradesByUserId_noTradesMatchedWithUser() {
        when(tradeServiceMock.getTradesByUserId(555L)).thenReturn(emptyList());

        final List<TradeApi> actualTradeApis = tradesController.getTradesByUserId(555L);

        verify(tradeServiceMock).getTradesByUserId(555L);
        verifyNoMoreInteractions(tradeServiceMock);

        assertThat(actualTradeApis).isEmpty();
    }

    @Test
    public void getTradesByUserId_notExistingUser() {
        when(tradeServiceMock.getTradesByUserId(555L)).thenThrow(new UserWasNotFoundException("No user was found"));

        assertThrows(UserWasNotFoundException.class, () -> tradesController.getTradesByUserId(555L));

        verify(tradeServiceMock).getTradesByUserId(555L);
        verifyNoMoreInteractions(tradeServiceMock);
    }

    @Test
    public void createNewTrade() {
        final TradeApi tradeApiToCreate = TRADE_API_1.toBuilder().id(null).tradeTimestamp(null).build();

        when(tradeServiceMock.createNewTrade(tradeApiToCreate)).thenReturn(TRADE_API_1);

        final TradeApi actualTradeApi = tradesController.createNewTrade(tradeApiToCreate);

        verify(tradeServiceMock).createNewTrade(tradeApiToCreate);
        verifyNoMoreInteractions(tradeServiceMock);

        assertThat(actualTradeApi).isEqualTo(TRADE_API_1);
    }

    @Test
    public void deleteAllTrades() {
        tradesController.deleteAllTrades();

        verify(tradeServiceMock).deleteAllTrades();
        verifyNoMoreInteractions(tradeServiceMock);
    }
}
