package com.skrypnyk.stocks.validation;

import static com.skrypnyk.stocks.TestData.TRADE_API_1;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import com.skrypnyk.stocks.entity.api.TradeApi;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * Represent test for {@link TradeApi} validations
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 **/
@ExtendWith(MockitoExtension.class)
public class TradeApiValidations {

    private static ValidatorFactory validatorFactory;
    private static Validator validator;

    @BeforeAll
    public static void createValidator() {
        validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @AfterAll
    public static void close() {
        validatorFactory.close();
    }

    @Test
    public void validateTradeApi_noViolations() {
        final TradeApi tradeApi = TRADE_API_1.toBuilder().id(null).tradeTimestamp(null).build();

        final Set<ConstraintViolation<TradeApi>> violations = validator.validate(tradeApi);

        assertThat(violations).isEmpty();
    }

    @Test
    public void validateTradeApi_withoutType() {
        final TradeApi tradeApi = TRADE_API_1.toBuilder().id(null).tradeType(null).tradeTimestamp(null).build();

        final Set<ConstraintViolation<TradeApi>> violations = validator.validate(tradeApi);
        final ConstraintViolation<TradeApi> violation = violations.iterator().next();

        assertThat(violations).hasSize(1);
        assertThat(violation.getMessage()).isEqualTo("type field is required");
    }

    @Test
    public void validateTradeApi_withoutUser() {
        final TradeApi tradeApi = TRADE_API_1.toBuilder().id(null).userApi(null).tradeTimestamp(null).build();

        final Set<ConstraintViolation<TradeApi>> violations = validator.validate(tradeApi);
        final ConstraintViolation<TradeApi> violation = violations.iterator().next();

        assertThat(violations).hasSize(1);
        assertThat(violation.getMessage()).isEqualTo("user field is required");
    }

    @Test
    public void validateTradeApi_withoutStockSymbol() {
        final TradeApi tradeApi = TRADE_API_1.toBuilder().id(null).stockSymbol(null).tradeTimestamp(null).build();

        final Set<ConstraintViolation<TradeApi>> violations = validator.validate(tradeApi);
        final ConstraintViolation<TradeApi> violation = violations.iterator().next();

        assertThat(violations).hasSize(1);
        assertThat(violation.getMessage()).isEqualTo("stock_symbol field is required");
    }

    @Test
    public void validateTradeApi_stockQuantity_LessThen10() {
        final TradeApi tradeApi = TRADE_API_1.toBuilder().id(null).stockQuantity(9).tradeTimestamp(null).build();

        final Set<ConstraintViolation<TradeApi>> violations = validator.validate(tradeApi);
        final ConstraintViolation<TradeApi> violation = violations.iterator().next();

        assertThat(violations).hasSize(1);
        assertThat(violation.getMessage()).isEqualTo("stock_quantity should be more than 10");
    }

    @Test
    public void validateTradeApi_stockQuantity_10() {
        final TradeApi tradeApi = TRADE_API_1.toBuilder().id(null).stockQuantity(10).tradeTimestamp(null).build();

        final Set<ConstraintViolation<TradeApi>> violations = validator.validate(tradeApi);

        assertThat(violations).isEmpty();
    }

    @Test
    public void validateTradeApi_stockQuantity_15() {
        final TradeApi tradeApi = TRADE_API_1.toBuilder().id(null).stockQuantity(15).tradeTimestamp(null).build();

        final Set<ConstraintViolation<TradeApi>> violations = validator.validate(tradeApi);

        assertThat(violations).isEmpty();
    }

    @Test
    public void validateTradeApi_stockQuantity_30() {
        final TradeApi tradeApi = TRADE_API_1.toBuilder().id(null).stockQuantity(30).tradeTimestamp(null).build();

        final Set<ConstraintViolation<TradeApi>> violations = validator.validate(tradeApi);

        assertThat(violations).isEmpty();
    }

    @Test
    public void validateTradeApi_stockQuantity_MoreThen30() {
        final TradeApi tradeApi = TRADE_API_1.toBuilder().id(null).stockQuantity(31).tradeTimestamp(null).build();

        final Set<ConstraintViolation<TradeApi>> violations = validator.validate(tradeApi);
        final ConstraintViolation<TradeApi> violation = violations.iterator().next();

        assertThat(violations).hasSize(1);
        assertThat(violation.getMessage()).isEqualTo("stock_quantity should be less than 30");
    }

    @Test
    public void validateTradeApi_stockPrice_LessThen130Point42() {
        final TradeApi tradeApi = TRADE_API_1.toBuilder().id(null).stockPrice(130.41).tradeTimestamp(null).build();

        final Set<ConstraintViolation<TradeApi>> violations = validator.validate(tradeApi);
        final ConstraintViolation<TradeApi> violation = violations.iterator().next();

        assertThat(violations).hasSize(1);
        assertThat(violation.getMessage()).isEqualTo("stock_price should be more than 130.42");
    }

    @Test
    public void validateTradeApi_stockQuantity_130Point42() {
        final TradeApi tradeApi = TRADE_API_1.toBuilder().id(null).stockPrice(130.42).tradeTimestamp(null).build();

        final Set<ConstraintViolation<TradeApi>> violations = validator.validate(tradeApi);

        assertThat(violations).isEmpty();
    }

    @Test
    public void validateTradeApi_stockQuantity_140() {
        final TradeApi tradeApi = TRADE_API_1.toBuilder().id(null).stockPrice(140).tradeTimestamp(null).build();

        final Set<ConstraintViolation<TradeApi>> violations = validator.validate(tradeApi);

        assertThat(violations).isEmpty();
    }

    @Test
    public void validateTradeApi_stockQuantity_195Point65() {
        final TradeApi tradeApi = TRADE_API_1.toBuilder().id(null).stockPrice(195.65).tradeTimestamp(null).build();

        final Set<ConstraintViolation<TradeApi>> violations = validator.validate(tradeApi);

        assertThat(violations).isEmpty();
    }

    @Test
    public void validateTradeApi_stockQuantity_MoreThen195Point65() {
        final TradeApi tradeApi = TRADE_API_1.toBuilder().id(null).stockPrice(195.66).tradeTimestamp(null).build();

        final Set<ConstraintViolation<TradeApi>> violations = validator.validate(tradeApi);
        final ConstraintViolation<TradeApi> violation = violations.iterator().next();

        assertThat(violations).hasSize(1);
        assertThat(violation.getMessage()).isEqualTo("stock_price should be less than 195.65");
    }
}
