package com.skrypnyk.stocks;

import static com.skrypnyk.stocks.entity.enums.TradeType.BUY;
import static com.skrypnyk.stocks.entity.enums.TradeType.SELL;
import static com.skrypnyk.stocks.util.DateUtil.buildDate;

import com.skrypnyk.stocks.entity.api.StockPriceApi;
import com.skrypnyk.stocks.entity.api.TradeApi;
import com.skrypnyk.stocks.entity.api.UserApi;
import com.skrypnyk.stocks.entity.db.TradeDb;
import com.skrypnyk.stocks.entity.db.UserDb;

/**
 * {@author maksymskrypnyk} since 0.1.0
 **/
public class TestData {

    public static final UserApi USER_API_1;
    public static final UserApi USER_API_2;
    public static final UserApi USER_API_3;

    public static final TradeApi TRADE_API_1;
    public static final TradeApi TRADE_API_2;
    public static final TradeApi TRADE_API_3;
    public static final TradeApi TRADE_API_4;

    public static final UserDb USER_DB_1;
    public static final UserDb USER_DB_2;
    public static final UserDb USER_DB_3;

    public static final TradeDb TRADE_DB_1;
    public static final TradeDb TRADE_DB_2;
    public static final TradeDb TRADE_DB_3;
    public static final TradeDb TRADE_DB_4;

    public static final StockPriceApi STOCK_PRICE_API_1;
    public static final StockPriceApi STOCK_PRICE_API_2;
    public static final StockPriceApi STOCK_PRICE_API_3;

    static {
        USER_API_1 = UserApi.builder()
                            .id(1L)
                            .name("FirstTestUser")
                            .build();

        USER_API_2 = UserApi.builder()
                            .id(2L)
                            .name("SecondTestUser")
                            .build();

        USER_API_3 = UserApi.builder()
                            .id(3L)
                            .name("ThirdTestUser")
                            .build();
    }

    static {
        USER_DB_1 = UserDb.builder()
                          .id(1L)
                          .name("FirstTestUser")
                          .build();

        USER_DB_2 = UserDb.builder()
                          .id(2L)
                          .name("SecondTestUser")
                          .build();

        USER_DB_3 = UserDb.builder()
                          .id(3L)
                          .name("ThirdTestUser")
                          .build();
    }

    static {
        TRADE_DB_1 = TradeDb.builder()
                            .id(1L)
                            .tradeType(BUY)
                            .userId(USER_DB_1.getId())
                            .stockSymbol("AC")
                            .stockQuantity(28)
                            .stockPrice(138.17)
                            .tradeTimestamp(buildDate("2021-04-22 18:15:13"))
                            .build();

        TRADE_DB_2 = TradeDb.builder()
                            .id(2L)
                            .tradeType(BUY)
                            .userId(USER_DB_1.getId())
                            .stockSymbol("AC")
                            .stockQuantity(13)
                            .stockPrice(152.26)
                            .tradeTimestamp(buildDate("2020-03-15 7:33:25"))
                            .build();

        TRADE_DB_3 = TradeDb.builder()
                            .id(3L)
                            .tradeType(BUY)
                            .userId(USER_DB_2.getId())
                            .stockSymbol("ACC")
                            .stockQuantity(23)
                            .stockPrice(175.33)
                            .tradeTimestamp(buildDate("2021-08-23 9:54:41"))
                            .build();

        TRADE_DB_4 = TradeDb.builder()
                            .id(4L)
                            .tradeType(SELL)
                            .userId(USER_DB_3.getId())
                            .stockSymbol("AC")
                            .stockQuantity(26)
                            .stockPrice(144.97)
                            .tradeTimestamp(buildDate("2019-01-7 16:45:9"))
                            .build();
    }

    static {
        TRADE_API_1 = TradeApi.builder()
                              .id(1L)
                              .tradeType(BUY)
                              .userApi(USER_API_1)
                              .stockSymbol("AC")
                              .stockQuantity(28)
                              .stockPrice(138.17)
                              .tradeTimestamp(buildDate("2021-04-22 18:15:13"))
                              .build();

        TRADE_API_2 = TradeApi.builder()
                              .id(2L)
                              .tradeType(BUY)
                              .userApi(USER_API_1)
                              .stockSymbol("AC")
                              .stockQuantity(13)
                              .stockPrice(152.26)
                              .tradeTimestamp(buildDate("2020-03-15 7:33:25"))
                              .build();

        TRADE_API_3 = TradeApi.builder()
                              .id(3L)
                              .tradeType(BUY)
                              .userApi(USER_API_2)
                              .stockSymbol("ACC")
                              .stockQuantity(23)
                              .stockPrice(175.33)
                              .tradeTimestamp(buildDate("2021-08-23 9:54:41"))
                              .build();

        TRADE_API_4 = TradeApi.builder()
                              .id(4L)
                              .tradeType(SELL)
                              .userApi(USER_API_3)
                              .stockSymbol("AC")
                              .stockQuantity(26)
                              .stockPrice(155.00)
                              .tradeTimestamp(buildDate("2019-01-7 16:45:9"))
                              .build();
    }

    static {
        STOCK_PRICE_API_1 = StockPriceApi.builder()
                                         .stockSymbol("AC")
                                         .lowestPrice(138.17)
                                         .highestPrice(152.26)
                                         .build();

        STOCK_PRICE_API_2 = StockPriceApi.builder()
                                         .stockSymbol("AC")
                                         .lowestPrice(152.26)
                                         .highestPrice(152.26)
                                         .build();

        STOCK_PRICE_API_3 = StockPriceApi.builder()
                                         .stockSymbol("ACC")
                                         .lowestPrice(175.33)
                                         .highestPrice(175.33)
                                         .build();
    }
}
