package com.skrypnyk.stocks.util;

import static com.skrypnyk.stocks.util.DateUtil.buildDate;
import static com.skrypnyk.stocks.util.DateUtil.isWithinRange;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * {@author maksymskrypnyk} since 0.1.0
 **/
@ExtendWith(MockitoExtension.class)
public class DateUtilTest {

    @Test
    public void isWithinRange_inRange_differentDays() {
        final Date dateToCheck = buildDate("2020-01-12 12:12:12");
        final Date fromDate = buildDate("2020-01-11 12:12:12");
        final Date toDate = buildDate("2020-01-13 12:12:12");

        final boolean actualResult = isWithinRange(dateToCheck, fromDate, toDate);

        assertThat(actualResult).isTrue();
    }

    @Test
    public void isWithinRange_inRange_differentTime() {
        final Date dateToCheck = buildDate("2020-01-12 12:12:12");
        final Date fromDate = buildDate("2020-01-12 12:11:12");
        final Date toDate = buildDate("2020-01-12 12:13:12");

        final boolean actualResult = isWithinRange(dateToCheck, fromDate, toDate);

        assertThat(actualResult).isTrue();
    }

    @Test
    public void isWithinRange_inRange_fromSameAsChecked() {
        final Date dateToCheck = buildDate("2020-01-11 12:12:12");
        final Date fromDate = buildDate("2020-01-11 12:12:12");
        final Date toDate = buildDate("2020-01-13 12:12:12");

        final boolean actualResult = isWithinRange(dateToCheck, fromDate, toDate);

        assertThat(actualResult).isTrue();
    }

    @Test
    public void isWithinRange_inRange_toSameAsChecked() {
        final Date dateToCheck = buildDate("2020-01-12 12:12:12");
        final Date fromDate = buildDate("2020-01-11 12:12:12");
        final Date toDate = buildDate("2020-01-12 12:12:12");

        final boolean actualResult = isWithinRange(dateToCheck, fromDate, toDate);

        assertThat(actualResult).isTrue();
    }

    @Test
    public void isWithRange_outOfRange_outOfFromDay() {
        final Date dateToCheck = buildDate("2020-01-10 12:12:12");
        final Date fromDate = buildDate("2020-01-11 12:12:12");
        final Date toDate = buildDate("2020-01-12 12:12:12");

        final boolean actualResult = isWithinRange(dateToCheck, fromDate, toDate);

        assertThat(actualResult).isFalse();
    }

    @Test
    public void isWithRange_outOfRange_outOfFromTime() {
        final Date dateToCheck = buildDate("2020-01-10 12:11:12");
        final Date fromDate = buildDate("2020-01-10 12:12:12");
        final Date toDate = buildDate("2020-01-12 12:12:12");

        final boolean actualResult = isWithinRange(dateToCheck, fromDate, toDate);

        assertThat(actualResult).isFalse();
    }

    @Test
    public void isWithRange_outOfRange_outOfToDay() {
        final Date dateToCheck = buildDate("2020-01-13 12:12:12");
        final Date fromDate = buildDate("2020-01-11 12:12:12");
        final Date toDate = buildDate("2020-01-12 12:12:12");

        final boolean actualResult = isWithinRange(dateToCheck, fromDate, toDate);

        assertThat(actualResult).isFalse();
    }

    @Test
    public void isWithRange_outOfRange_outOfToTime() {
        final Date dateToCheck = buildDate("2020-01-12 12:12:12");
        final Date fromDate = buildDate("2020-01-11 12:12:12");
        final Date toDate = buildDate("2020-01-12 12:11:12");

        final boolean actualResult = isWithinRange(dateToCheck, fromDate, toDate);

        assertThat(actualResult).isFalse();
    }
}
