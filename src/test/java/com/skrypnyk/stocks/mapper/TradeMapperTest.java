package com.skrypnyk.stocks.mapper;

import static com.skrypnyk.stocks.TestData.TRADE_API_1;
import static com.skrypnyk.stocks.TestData.TRADE_API_2;
import static com.skrypnyk.stocks.TestData.TRADE_API_3;
import static com.skrypnyk.stocks.TestData.TRADE_DB_1;
import static com.skrypnyk.stocks.TestData.TRADE_DB_2;
import static com.skrypnyk.stocks.TestData.TRADE_DB_3;
import static com.skrypnyk.stocks.TestData.USER_API_1;
import static com.skrypnyk.stocks.TestData.USER_API_2;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.List;

import com.skrypnyk.stocks.entity.api.TradeApi;
import com.skrypnyk.stocks.entity.db.TradeDb;
import com.skrypnyk.stocks.entity.mapper.TradeMapper;
import com.skrypnyk.stocks.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * {@author maksymskrypnyk} since 0.1.0
 **/
@ExtendWith(MockitoExtension.class)
public class TradeMapperTest {

    @InjectMocks
    private TradeMapper tradeMapper;

    @Mock
    private UserService userService;

    @Test
    public void toTradeApi() {
        when(userService.getUserById(1L)).thenReturn(USER_API_1);

        final TradeApi actualTradeApi = tradeMapper.toTradeApi(TRADE_DB_1);

        verify(userService).getUserById(1L);
        verifyNoMoreInteractions(userService);

        assertThat(actualTradeApi).isEqualTo(TRADE_API_1);
    }

    @Test
    public void toTradeApi_severalTradeDbs() {
        when(userService.getUserById(1L)).thenReturn(USER_API_1);
        when(userService.getUserById(2L)).thenReturn(USER_API_2);

        final List<TradeApi> actualTradeApis = tradeMapper.toTradeApi(asList(TRADE_DB_1, TRADE_DB_2, TRADE_DB_3));

        verify(userService, times(2)).getUserById(1L);
        verify(userService).getUserById(2L);
        verifyNoMoreInteractions(userService);

        assertThat(actualTradeApis).isEqualTo(asList(TRADE_API_1, TRADE_API_2, TRADE_API_3));
    }

    @Test
    public void toTradeDb() {
        final TradeDb actualTradeDb = tradeMapper.toTradeDb(TRADE_API_1);

        assertThat(actualTradeDb).isEqualTo(TRADE_DB_1.toBuilder().tradeTimestamp(null).build());
    }

    @Test
    public void toTradeDb_withStockPriceRound() {
        final TradeDb actualTradeDb = tradeMapper.toTradeDb(TRADE_API_1.toBuilder().stockPrice(138.17123123).build());

        assertThat(actualTradeDb).isEqualTo(TRADE_DB_1.toBuilder().tradeTimestamp(null).build());
    }
}
