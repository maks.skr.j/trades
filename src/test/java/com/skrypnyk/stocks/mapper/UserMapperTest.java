package com.skrypnyk.stocks.mapper;

import static com.skrypnyk.stocks.TestData.USER_API_1;
import static com.skrypnyk.stocks.TestData.USER_DB_1;
import static org.assertj.core.api.Assertions.assertThat;

import com.skrypnyk.stocks.entity.api.UserApi;
import com.skrypnyk.stocks.entity.db.UserDb;
import com.skrypnyk.stocks.entity.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * {@author maksymskrypnyk} since 0.1.0
 **/
@ExtendWith(MockitoExtension.class)
public class UserMapperTest {

    @InjectMocks
    private UserMapper userMapper;

    @Test
    public void toTradeApi() {
        final UserApi actualUserApi = userMapper.toUserApi(USER_DB_1);

        assertThat(actualUserApi).isEqualTo(USER_API_1);
    }

    @Test
    public void toTradeDb() {
        final UserDb actualUserDb = userMapper.toUserDb(USER_API_1);

        assertThat(actualUserDb).isEqualTo(USER_DB_1);
    }
}
