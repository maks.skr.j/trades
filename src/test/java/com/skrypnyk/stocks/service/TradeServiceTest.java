package com.skrypnyk.stocks.service;

import static com.skrypnyk.stocks.TestData.STOCK_PRICE_API_1;
import static com.skrypnyk.stocks.TestData.STOCK_PRICE_API_2;
import static com.skrypnyk.stocks.TestData.STOCK_PRICE_API_3;
import static com.skrypnyk.stocks.TestData.TRADE_API_1;
import static com.skrypnyk.stocks.TestData.TRADE_API_2;
import static com.skrypnyk.stocks.TestData.TRADE_API_3;
import static com.skrypnyk.stocks.TestData.TRADE_API_4;
import static com.skrypnyk.stocks.TestData.TRADE_DB_1;
import static com.skrypnyk.stocks.TestData.TRADE_DB_2;
import static com.skrypnyk.stocks.TestData.TRADE_DB_3;
import static com.skrypnyk.stocks.TestData.TRADE_DB_4;
import static com.skrypnyk.stocks.entity.enums.TradeType.BUY;
import static com.skrypnyk.stocks.util.DateUtil.buildDate;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.List;

import com.skrypnyk.stocks.dal.dao.TradeDao;
import com.skrypnyk.stocks.entity.api.StockPriceApi;
import com.skrypnyk.stocks.entity.api.TradeApi;
import com.skrypnyk.stocks.entity.db.TradeDb;
import com.skrypnyk.stocks.entity.mapper.TradeMapper;
import com.skrypnyk.stocks.exceptions.DateRangeException;
import com.skrypnyk.stocks.exceptions.UserWasNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * Represent tests for {@link TradeService} functionality
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 **/
@ExtendWith(MockitoExtension.class)
public class TradeServiceTest {

    @InjectMocks
    private TradeService tradeService;

    @Mock
    private TradeDao tradeDaoMock;
    @Mock
    private TradeMapper tradeMapperMock;

    @Test
    public void getAllTrades() {
        final List<TradeDb> tradeDbs = asList(TRADE_DB_1, TRADE_DB_2, TRADE_DB_3, TRADE_DB_4);
        final List<TradeApi> tradeApis = asList(TRADE_API_1, TRADE_API_2, TRADE_API_3, TRADE_API_4);

        when(tradeDaoMock.getAllTrades()).thenReturn(tradeDbs);
        when(tradeMapperMock.toTradeApi(tradeDbs)).thenReturn(tradeApis);

        final List<TradeApi> actualTradeApis = tradeService.getAllTrades();

        verify(tradeDaoMock).getAllTrades();
        verify(tradeMapperMock).toTradeApi(tradeDbs);
        verifyNoMoreInteractions(tradeDaoMock, tradeMapperMock);

        assertThat(actualTradeApis).isEqualTo(tradeApis);
    }

    @Test
    public void getAllTrades_oneTradeInSystem() {
        final List<TradeDb> tradeDbs = singletonList(TRADE_DB_1);
        final List<TradeApi> tradeApis = singletonList(TRADE_API_1);

        when(tradeDaoMock.getAllTrades()).thenReturn(tradeDbs);
        when(tradeMapperMock.toTradeApi(tradeDbs)).thenReturn(tradeApis);

        final List<TradeApi> actualTradeApis = tradeService.getAllTrades();

        verify(tradeDaoMock).getAllTrades();
        verify(tradeMapperMock).toTradeApi(tradeDbs);
        verifyNoMoreInteractions(tradeDaoMock, tradeMapperMock);

        assertThat(actualTradeApis).isEqualTo(tradeApis);
    }

    @Test
    public void getAllTrades_noTradesInSystem() {
        final List<TradeDb> tradeDbs = emptyList();
        final List<TradeApi> tradeApis = emptyList();

        when(tradeDaoMock.getAllTrades()).thenReturn(tradeDbs);
        when(tradeMapperMock.toTradeApi(tradeDbs)).thenReturn(tradeApis);

        final List<TradeApi> actualTradeApis = tradeService.getAllTrades();

        verify(tradeDaoMock).getAllTrades();
        verify(tradeMapperMock).toTradeApi(tradeDbs);
        verifyNoMoreInteractions(tradeDaoMock, tradeMapperMock);

        assertThat(actualTradeApis).isEqualTo(tradeApis);
    }

    @Test
    public void getTradesByUserId_oneUser() {
        final Long userId = 2L;
        final List<TradeDb> tradeDbs = singletonList(TRADE_DB_3);
        final List<TradeApi> tradeApis = singletonList(TRADE_API_3);

        when(tradeDaoMock.getTradesByUserId(TRADE_DB_3.getUserId())).thenReturn(tradeDbs);
        when(tradeMapperMock.toTradeApi(tradeDbs)).thenReturn(tradeApis);

        final List<TradeApi> actualTradeApis = tradeService.getTradesByUserId(userId);

        verify(tradeDaoMock).getTradesByUserId(userId);
        verify(tradeMapperMock).toTradeApi(tradeDbs);
        verifyNoMoreInteractions(tradeDaoMock, tradeMapperMock);

        assertThat(actualTradeApis).isEqualTo(tradeApis);
    }

    @Test
    public void getTradesByUserId_severalUsers() {
        final Long userId = 1L;
        final List<TradeDb> tradeDbs = asList(TRADE_DB_1, TRADE_DB_2);
        final List<TradeApi> tradeApis = asList(TRADE_API_1, TRADE_API_2);

        when(tradeDaoMock.getTradesByUserId(userId)).thenReturn(tradeDbs);
        when(tradeMapperMock.toTradeApi(tradeDbs)).thenReturn(tradeApis);

        final List<TradeApi> actualTradeApis = tradeService.getTradesByUserId(userId);

        verify(tradeDaoMock).getTradesByUserId(userId);
        verify(tradeMapperMock).toTradeApi(tradeDbs);
        verifyNoMoreInteractions(tradeDaoMock, tradeMapperMock);

        assertThat(actualTradeApis).isEqualTo(tradeApis);
    }

    @Test
    public void getTradesByUserId_userNotMatchedWithAnyTrade() {
        final Long userId = 1L;
        final List<TradeDb> tradeDbs = emptyList();
        final List<TradeApi> tradeApis = emptyList();


        when(tradeDaoMock.getTradesByUserId(userId)).thenReturn(tradeDbs);
        when(tradeMapperMock.toTradeApi(tradeDbs)).thenReturn(tradeApis);

        final List<TradeApi> actualTradeApis = tradeService.getTradesByUserId(userId);

        verify(tradeDaoMock).getTradesByUserId(userId);
        verify(tradeMapperMock).toTradeApi(tradeDbs);
        verifyNoMoreInteractions(tradeDaoMock, tradeMapperMock);

        assertThat(actualTradeApis).isEmpty();
    }

    @Test
    public void getTradesByUserId_notExistingUser() throws UserWasNotFoundException {
        final Long notExistingUserId = 55L;

        when(tradeDaoMock.getTradesByUserId(notExistingUserId))
                .thenThrow(new UserWasNotFoundException("User was not found"));

        assertThrows(UserWasNotFoundException.class, () -> tradeService.getTradesByUserId(notExistingUserId));

        verify(tradeDaoMock).getTradesByUserId(notExistingUserId);
        verifyNoInteractions(tradeMapperMock);
        verifyNoMoreInteractions(tradeDaoMock, tradeMapperMock);
    }

    @Test
    public void getTradesByParam_oneTradeMatched() {
        final List<TradeDb> tradeDbs = asList(TRADE_DB_1, TRADE_DB_2);

        when(tradeDaoMock.getTradesByStockSymbolAndTradeType("AC", BUY)).thenReturn(tradeDbs);
        when(tradeMapperMock.toTradeApi(tradeDbs)).thenReturn(asList(TRADE_API_1, TRADE_API_2));

        final Date from = buildDate("2020-01-12 12:12:12");
        final Date to = buildDate("2020-12-12 12:12:12");

        final List<TradeApi> actualTradeApis = tradeService.getTradesByParams("AC", BUY, from, to);

        verify(tradeDaoMock).getTradesByStockSymbolAndTradeType("AC", BUY);
        verify(tradeMapperMock).toTradeApi(tradeDbs);
        verifyNoMoreInteractions(tradeDaoMock, tradeMapperMock);

        assertThat(actualTradeApis).isEqualTo(singletonList(TRADE_API_2));
    }

    @Test
    public void getTradesByParam_severalTradesMatched() {
        final List<TradeDb> tradeDbs = asList(TRADE_DB_1, TRADE_DB_2);
        final List<TradeApi> tradeApis = asList(TRADE_API_1, TRADE_API_2);

        when(tradeDaoMock.getTradesByStockSymbolAndTradeType("AC", BUY)).thenReturn(tradeDbs);
        when(tradeMapperMock.toTradeApi(tradeDbs)).thenReturn(tradeApis);

        final Date from = buildDate("2020-01-12 12:12:12");
        final Date to = buildDate("2022-12-12 12:12:12");
        final List<TradeApi> actualTradeApis = tradeService.getTradesByParams("AC", BUY, from, to);

        verify(tradeDaoMock).getTradesByStockSymbolAndTradeType("AC", BUY);
        verify(tradeMapperMock).toTradeApi(tradeDbs);
        verifyNoMoreInteractions(tradeDaoMock, tradeMapperMock);

        assertThat(actualTradeApis).isEqualTo(tradeApis);
    }

    @Test
    public void getTradesByParam_noTradesByStockSymbol() {
        when(tradeDaoMock.getTradesByStockSymbolAndTradeType("DC", BUY)).thenReturn(emptyList());

        final Date from = buildDate("2020-01-12 12:12:12");
        final Date to = buildDate("2022-12-12 12:12:12");
        final List<TradeApi> actualTradeApis = tradeService.getTradesByParams("DC", BUY, from, to);

        verify(tradeDaoMock).getTradesByStockSymbolAndTradeType("DC", BUY);
        verifyNoInteractions(tradeMapperMock);
        verifyNoMoreInteractions(tradeDaoMock);

        assertThat(actualTradeApis).isEmpty();
    }

    @Test
    public void getTradesByParam_noTradesByTradeType() {
        when(tradeDaoMock.getTradesByStockSymbolAndTradeType("AC", BUY)).thenReturn(emptyList());

        final Date from = buildDate("2020-01-12 12:12:12");
        final Date to = buildDate("2022-12-12 12:12:12");
        final List<TradeApi> actualTradeApis = tradeService.getTradesByParams("AC", BUY, from, to);

        verify(tradeDaoMock).getTradesByStockSymbolAndTradeType("AC", BUY);
        verifyNoInteractions(tradeMapperMock);
        verifyNoMoreInteractions(tradeDaoMock);

        assertThat(actualTradeApis).isEmpty();
    }

    @Test
    public void getTradesByParam_noTradesInDateRange() {
        when(tradeDaoMock.getTradesByStockSymbolAndTradeType("AC", BUY))
                .thenReturn(asList(TRADE_DB_1, TRADE_DB_2));

        final Date from = buildDate("2020-01-12 12:12:12");
        final Date to = buildDate("2020-01-12 12:12:14");
        assertThrows(DateRangeException.class, () -> tradeService.getTradesByParams("AC", BUY, from, to));

        verify(tradeDaoMock).getTradesByStockSymbolAndTradeType("AC", BUY);
        verify(tradeMapperMock).toTradeApi(asList(TRADE_DB_1, TRADE_DB_2));
        verifyNoMoreInteractions(tradeDaoMock);
    }

    @Test
    public void getStockPriceBySymbolInDateRange_oneTradeMatched() {
        when(tradeDaoMock.getStockBySymbol("ACC")).thenReturn(singletonList(TRADE_DB_3));
        when(tradeMapperMock.toTradeApi(singletonList(TRADE_DB_3))).thenReturn(singletonList(TRADE_API_3));

        final Date from = buildDate("2020-01-12 12:12:12");
        final Date to = buildDate("2022-12-12 12:12:12");
        final StockPriceApi actualStockPriceApi = tradeService.getStockPriceBySymbolInDateRange("ACC", from, to);

        verify(tradeDaoMock).getStockBySymbol("ACC");
        verify(tradeMapperMock).toTradeApi(singletonList(TRADE_DB_3));
        verifyNoMoreInteractions(tradeDaoMock, tradeMapperMock);

        assertThat(actualStockPriceApi).isEqualTo(STOCK_PRICE_API_3);
    }

    @Test
    public void getStockPriceBySymbolInDateRange_oneTradeMatchedByDate() {
        when(tradeDaoMock.getStockBySymbol("AC")).thenReturn(asList(TRADE_DB_1, TRADE_DB_2, TRADE_DB_4));
        when(tradeMapperMock.toTradeApi(asList(TRADE_DB_1, TRADE_DB_2, TRADE_DB_4)))
                .thenReturn(asList(TRADE_API_1, TRADE_API_2, TRADE_API_4));

        final Date from = buildDate("2020-01-01 12:12:12");
        final Date to = buildDate("2020-12-30 12:12:12");
        final StockPriceApi actualStockPriceApi = tradeService.getStockPriceBySymbolInDateRange("AC", from, to);

        verify(tradeDaoMock).getStockBySymbol("AC");
        verify(tradeMapperMock).toTradeApi(asList(TRADE_DB_1, TRADE_DB_2, TRADE_DB_4));
        verifyNoMoreInteractions(tradeDaoMock, tradeMapperMock);

        assertThat(actualStockPriceApi).isEqualTo(STOCK_PRICE_API_2);
    }

    @Test
    public void getStockPriceBySymbolInDateRange_severalTradesMatched() {
        when(tradeDaoMock.getStockBySymbol("AC")).thenReturn(asList(TRADE_DB_1, TRADE_DB_2, TRADE_DB_4));
        when(tradeMapperMock.toTradeApi(asList(TRADE_DB_1, TRADE_DB_2, TRADE_DB_4)))
                .thenReturn(asList(TRADE_API_1, TRADE_API_2, TRADE_API_4));

        final Date from = buildDate("2020-01-01 12:12:12");
        final Date to = buildDate("2021-12-30 12:12:12");
        final StockPriceApi actualStockPriceApi = tradeService.getStockPriceBySymbolInDateRange("AC", from, to);

        verify(tradeDaoMock).getStockBySymbol("AC");
        verify(tradeMapperMock).toTradeApi(asList(TRADE_DB_1, TRADE_DB_2, TRADE_DB_4));
        verifyNoMoreInteractions(tradeDaoMock, tradeMapperMock);

        assertThat(actualStockPriceApi).isEqualTo(STOCK_PRICE_API_1);
    }

    @Test
    public void getStockPriceBySymbolInDateRange_noTradesMatchedByStockSymbol() {
        when(tradeDaoMock.getStockBySymbol("DC")).thenReturn(emptyList());
        when(tradeMapperMock.toTradeApi(emptyList())).thenReturn(emptyList());

        final Date from = buildDate("2000-01-01 12:12:12");
        final Date to = buildDate("2030-12-30 12:12:12");

        assertThrows(DateRangeException.class, () -> tradeService.getStockPriceBySymbolInDateRange("DC", from, to));

        verify(tradeDaoMock).getStockBySymbol("DC");
        verify(tradeMapperMock).toTradeApi(emptyList());
        verifyNoMoreInteractions(tradeDaoMock, tradeMapperMock);
    }

    @Test
    public void getStockPriceBySymbolInDateRange_noTradesMatchedByDateRange() {
        when(tradeDaoMock.getStockBySymbol("AC")).thenReturn(asList(TRADE_DB_1, TRADE_DB_2, TRADE_DB_4));
        when(tradeMapperMock.toTradeApi(asList(TRADE_DB_1, TRADE_DB_2, TRADE_DB_4)))
                .thenReturn(asList(TRADE_API_1, TRADE_API_2, TRADE_API_4));

        final Date from = buildDate("2020-01-01 12:12:12");
        final Date to = buildDate("2020-01-01 12:12:20");

        assertThrows(DateRangeException.class, () -> tradeService.getStockPriceBySymbolInDateRange("AC", from, to));

        verify(tradeDaoMock).getStockBySymbol("AC");
        verify(tradeMapperMock).toTradeApi(asList(TRADE_DB_1, TRADE_DB_2, TRADE_DB_4));
        verifyNoMoreInteractions(tradeDaoMock, tradeMapperMock);
    }

    @Test
    public void createNewTrade() {
        final TradeDb tradeDbToCreate = TRADE_DB_1.toBuilder().id(null).tradeTimestamp(null).build();
        final TradeApi tradeApiToCreate = TRADE_API_1.toBuilder().id(null).tradeTimestamp(null).build();

        when(tradeDaoMock.save(tradeDbToCreate)).thenReturn(TRADE_DB_1);
        when(tradeMapperMock.toTradeDb(tradeApiToCreate)).thenReturn(tradeDbToCreate);
        when(tradeMapperMock.toTradeApi(TRADE_DB_1)).thenReturn(TRADE_API_1);

        final TradeApi actualTradeApi = tradeService.createNewTrade(tradeApiToCreate);

        verify(tradeDaoMock).save(tradeDbToCreate);
        verify(tradeMapperMock).toTradeApi(TRADE_DB_1);
        verify(tradeMapperMock).toTradeDb(tradeApiToCreate);
        verifyNoMoreInteractions(tradeDaoMock, tradeMapperMock);

        assertThat(actualTradeApi).isEqualTo(TRADE_API_1);
    }

    @Test
    public void deleteAllTrades() {
        tradeService.deleteAllTrades();

        verify(tradeDaoMock).deleteAll();
        verifyNoMoreInteractions(tradeDaoMock, tradeMapperMock);
    }
}
