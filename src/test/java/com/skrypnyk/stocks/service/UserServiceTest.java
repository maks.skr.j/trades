package com.skrypnyk.stocks.service;

import static com.skrypnyk.stocks.TestData.USER_API_1;
import static com.skrypnyk.stocks.TestData.USER_DB_1;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Optional;

import com.skrypnyk.stocks.dal.dao.UserDao;
import com.skrypnyk.stocks.entity.api.UserApi;
import com.skrypnyk.stocks.entity.mapper.UserMapper;
import com.skrypnyk.stocks.exceptions.UserWasNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * Represent tests for {@link UserService} functionality
 * <p>
 * {@author maksymskrypnyk} since 0.1.0
 **/
@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserDao userDaoMock;
    @Mock
    private UserMapper userMapperMock;

    @Test
    public void getUserById() {
        when(userDaoMock.findById(1L)).thenReturn(Optional.ofNullable(USER_DB_1));
        when(userMapperMock.toUserApi(USER_DB_1)).thenReturn(USER_API_1);

        final UserApi actualUserApi = userService.getUserById(1L);

        verify(userDaoMock).findById(1L);
        verify(userMapperMock).toUserApi(USER_DB_1);
        verifyNoMoreInteractions(userDaoMock, userMapperMock);

        assertThat(actualUserApi).isEqualTo(USER_API_1);
    }

    @Test
    public void getUserById_notExistingUser() {
        when(userDaoMock.findById(555L)).thenReturn(Optional.empty());

        assertThrows(UserWasNotFoundException.class, () -> userService.getUserById(555L));

        verify(userDaoMock).findById(555L);
        verifyNoMoreInteractions(userDaoMock, userMapperMock);
    }
}
